// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LAB7_Lab7GameModeBase_generated_h
#error "Lab7GameModeBase.generated.h already included, missing '#pragma once' in Lab7GameModeBase.h"
#endif
#define LAB7_Lab7GameModeBase_generated_h

#define Lab7_Source_Lab7_Lab7GameModeBase_h_15_SPARSE_DATA
#define Lab7_Source_Lab7_Lab7GameModeBase_h_15_RPC_WRAPPERS
#define Lab7_Source_Lab7_Lab7GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Lab7_Source_Lab7_Lab7GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALab7GameModeBase(); \
	friend struct Z_Construct_UClass_ALab7GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ALab7GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Lab7"), NO_API) \
	DECLARE_SERIALIZER(ALab7GameModeBase)


#define Lab7_Source_Lab7_Lab7GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesALab7GameModeBase(); \
	friend struct Z_Construct_UClass_ALab7GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ALab7GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Lab7"), NO_API) \
	DECLARE_SERIALIZER(ALab7GameModeBase)


#define Lab7_Source_Lab7_Lab7GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALab7GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALab7GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALab7GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALab7GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALab7GameModeBase(ALab7GameModeBase&&); \
	NO_API ALab7GameModeBase(const ALab7GameModeBase&); \
public:


#define Lab7_Source_Lab7_Lab7GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALab7GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALab7GameModeBase(ALab7GameModeBase&&); \
	NO_API ALab7GameModeBase(const ALab7GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALab7GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALab7GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALab7GameModeBase)


#define Lab7_Source_Lab7_Lab7GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Lab7_Source_Lab7_Lab7GameModeBase_h_12_PROLOG
#define Lab7_Source_Lab7_Lab7GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Lab7_Source_Lab7_Lab7GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Lab7_Source_Lab7_Lab7GameModeBase_h_15_SPARSE_DATA \
	Lab7_Source_Lab7_Lab7GameModeBase_h_15_RPC_WRAPPERS \
	Lab7_Source_Lab7_Lab7GameModeBase_h_15_INCLASS \
	Lab7_Source_Lab7_Lab7GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Lab7_Source_Lab7_Lab7GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Lab7_Source_Lab7_Lab7GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Lab7_Source_Lab7_Lab7GameModeBase_h_15_SPARSE_DATA \
	Lab7_Source_Lab7_Lab7GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Lab7_Source_Lab7_Lab7GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Lab7_Source_Lab7_Lab7GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LAB7_API UClass* StaticClass<class ALab7GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Lab7_Source_Lab7_Lab7GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
