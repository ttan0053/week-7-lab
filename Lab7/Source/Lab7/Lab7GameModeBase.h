// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lab7GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LAB7_API ALab7GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
